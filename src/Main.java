import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // prints out a string that says hello what's your name
        System.out.println("Hello! What is your name?");
        Scanner getName = new Scanner(System.in);
        String inputName = getName.nextLine();

        String playAgain = "y"; // we created a string named playAgain for users that type y
        while (playAgain.equals("y")) {

            System.out.println("Well, " + inputName + "," + " I am thinking of a number between 1 and 20.");
            System.out.println("Take a guess.");

            Random r = new Random(); // Random is an object we use for the secretNum
            int inputNum; // the number the user will type in
            int secretNum = r.nextInt(20) + 1; // no +1 will give us numbers from 0-19
            int guess = 1; // the starting guess is 1 for users
            boolean correctGuess = false; // create a boolean that says the correctGuess is false

            while (guess < 6 && !correctGuess) { //takes in conditional
                inputNum = getName.nextInt(); // gets the users number input
                if (inputNum == secretNum) { // if they match the number
                    System.out.println("Good Job, " + inputName + ", you guessed my number in " + guess + " guesses");
                    //for the inner while loop to continue it has to be true
                    correctGuess = true;
                }
                if (inputNum < secretNum) {
                    System.out.println("Your guess is too low.");
                    System.out.println("Take a guess.");
                } else if (inputNum > secretNum) {
                    System.out.println("Your guess is too high.");
                    System.out.println("Take a guess.");
                }
                guess++; // we increment by 1 to show how many guesses the user had
            }

            Scanner replay = new Scanner(System.in);
            System.out.println("Would you like to again? (y or n).");
            playAgain = replay.nextLine();
            System.out.println(playAgain);
        }
    }
}